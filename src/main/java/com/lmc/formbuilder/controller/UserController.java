package com.lmc.formbuilder.controller;

import java.util.Collection;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lmc.formbuilder.repository.UserRepository;
import com.lmc.formbuilder.model.User;

@RestController
public class UserController {

    @Autowired
    private UserRepository repository;

    @GetMapping(path="/users")
    public Collection<User> users() {
        return repository.findAll();
    }

    @GetMapping(path="/users/{id}")
    public User provider(@PathVariable String id) {
        return repository.findOne(id);
    }

    @PostMapping(path="/users")
    public User save(@Valid @RequestBody User user) {
        return repository.save(user);
    }
}