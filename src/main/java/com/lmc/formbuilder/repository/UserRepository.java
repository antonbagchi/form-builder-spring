package com.lmc.formbuilder.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.lmc.formbuilder.model.User;

public interface UserRepository extends MongoRepository<User, String> {
}