package com.lmc.formbuilder.model;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

public class User {

	@Id
	private String id;
	@NotNull
	private String firstName;
	@NotNull
	private String lastName;
	
	public User(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}

	// Think you need this for JSON conversion
	private User() 
	{}

	// Put in equals/hashcode/toString
}
