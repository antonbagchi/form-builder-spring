Simple Spring Boot REST example using Spring Data MongoDB.

Pre-requisites:
* Java 8
* Gradle 
* MongoDB installation

Run the following from the command line to start the service:
* ```gradle bootRun```
 
You can then access the following endpoints:
* GET http://localhost:8080/users - returns all users
* GET http://localhost:8080/users/{id} - returns user if exists, else empty
* POST http://localhost:8080/users - returns the saved user